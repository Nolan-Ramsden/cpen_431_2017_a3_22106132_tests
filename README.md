# Assignment 3
 
### Running The Tests
The test code is bundled into the `.jar` file, `Tests.jar`. To run the test suite, either pass the port as the first argument (host is set to localhost)
```
java -jar Tests.jar [port]
```

Or alternatively, use the `host:port` syntax
```
java -jar Tests.jar [host:port]
```

** If all tests fail with `Maximum retries of 3 reached`, this means that the tests can't find the server

** For all the tests to pass (Including the low memory recovery, the server must be running with very limited memory ~2Mb)

#### Test Descriptions

**testPutUntilOutOfSpace_shouldAllowPutAfterClear**

Spams the server with `PUT`s of 9Kb until the server returns `OutOfSpace`. It then sends a `clear` and expects subsequent `PUT`s will exceed. Make sure to run with `-Xmx2m` to make sure the `OutOfSpace` error is reached. If not, it will mark the test as inconclusive.

**testGetPIDReturnsSuccess**

Simply verifies that the `GetPID` command returns a non-zero response with a `Success` code.

**testPutUntilOutOfSpace_shouldAllowPutAfterClear**

Continues `Put(x:largeVal)` until the server returns `OutOfSpace` error code. It then sends `Clear` and expects `Put(x:;argeVal)` to succeed.

**testPutWithInvalidValue_shouldReturnInvalidValue**

Verifies that `Put` with a key > 10000 bytes returns the `InvalidValue` error.

**testPutThenGet_shouldReturnSuccessAndValue**

Verifies that a `Put(x:y)` then `Get(x)` returns `y`.

**testPutOverridesKey**

Verifies that a `Put(x:y)` then `Put(x:z)` then `Get(x)` returns `z`.

**testGetRemovePutWithInvalidKey_shouldReturnInvalidKey**

Verifies that all of  `Put`, `Get` and `Remove` with a key > 32 bytes returns `InvalidKey` error.

**testIdenticalMessageIdPut_shouldNotBeProcessed**

Verifies that a `Put(x:y)` then `Put(x:z)`(With identical messageID of `Put(x:y)` then `Get(x)` returns `y`.

**testClearAll_shouldReturnSuccess**

Simply verifies that the `DeleteAll` command returns a non-zero response (If this is failing, many other tests will fail).

**testPutThenGetThenRemove_shouldReturnNotFound**

Verifies that a `Put(x:y)` then `Remove(x)` then `Get(x)` returns `KeyNotFound` error.

**testIdenticalMessageIdClear_shouldNotBeProcessed**

Verifies that a `Put(x:y)` then `Clear`(With identical messageID of `Put(x:y)` then `Get(x)` returns `y`.

**testInvalidCommandCode_shouldReturnInvalidCommandError**

Verifies that command code 100, returns `UnknownCommand` error.

**testClearKeysThenGet_shouldReturnNotFound**

Verifies that a `Put(x)` then `Clear` then `Get(x)` returns `KeyNotFound` error.

**testClearThenRemove_shouldReturnNotFound**

Verifies that a `Put(x)` then `Clear` then `Remove(x)` returns `KeyNotFound` error.

**testIsAlive_shouldReturnSuccess**

Simply verifies that the `isAlive` returns a `Success` code.
